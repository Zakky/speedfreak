package com.ssh.speedfreak;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import java.io.InputStream;
import org.json.JSONObject;
import org.json.JSONArray;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.json.JSONException;
import android.content.Context;
import android.util.Log;
//import java.lang.Math;
import java.util.ArrayList;
import java.util.Random;

public class Map implements Ball.OnMoveListener {
    static Random random = new Random();

    public static Bitmap sevenBitmap; // セブンイレブンの画像
    public static Rect sevenRect;     // セブンイレブンの画像の大きさ


    private final int BLOCK_MARGIN_TOP = 1;
    private final int BLOCK_MARGIN_BOTTOM = 2;
    private final int BLOCK_MARGIN_LEFT = 1;
    private final int BLOCK_MARGIN_RIGHT = 2;

    private int blockSize;

    private int stageNum; // 全ステージ数
    private int blockNumX; // 横方向に存在するブロック数
    private int blockNumY; // 縦方向に存在するブロック数
    private int screenNumX; // 横方向に存在するスクリーン数
    private int screenNumY; // 縦方向に存在するスクリーン数

    private int curStage; // 現在のステージ番号
    //private int curBlockX;
    //private int curBlockY;
    private int curScreenX; // 現在のスクリーン番号（横）
    private int curScreenY; // 現在のスクリーン番号（縦）

    private int screenWidth;  // 描画可能エリアの画素数（横）
    private int screenHeight; // 描画可能エリアの画素数（縦）

    private JSONObject jsonObject;

    private Block[][][][] block;

    private final Block[][] targetBlock = new Block[3][3];
    private Block startBlock;

    private ArrayList<Block>[][] obstacleStart; // 自動車・自転車の出現位置
    private ArrayList<Block>[][] obstacleEnd;   // 自動車・自転車の消滅位置

    private Ball ball;

    public Block getStartBlock() { return startBlock; }
    public int getBlockSize() { return blockSize; }
    public int getBlockNumX() { return blockNumX; }
    public int getBlockNumY() { return blockNumY; }
    public int getScreenNumX() { return screenNumX; }
    public int getScreenNumY() { return screenNumY; }
    public int getCurScreenX() { return curScreenX; }
    public int getCurScreenY() { return curScreenY; }
    public void setBall(Ball b) { ball = b; }
    public int getBlockX(int x) { return x / blockSize; }
    public int getBlockY(int y) { return y / blockSize; }
    public int getBlockX(Block b) { return b.rect.left / blockSize; }
    public int getBlockY(Block b) { return b.rect.top / blockSize; }
    //public Block getBlock(int blockY, int blockX) { return block[curScreenY][curScreenX][blockY][blockX]; }
    public ArrayList<Block> getObstacleStart() { return obstacleStart[curScreenY][curScreenX]; }

    public double getMySpeed() {
        double answer = 3.0;
        try {
            if (!jsonObject.isNull("mySpeed"))
                answer = jsonObject.getDouble("mySpeed");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public double getMyBallScale() {
        double answer = 0.8;
        try {
            if (!jsonObject.isNull("myBallScale"))
                answer = jsonObject.getDouble("myBallScale");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public double getBikeDensity() {
        double answer = 0;
        try {
            if (!jsonObject.isNull("bikeDensity"))
                answer = jsonObject.getDouble("bikeDensity");
            JSONArray jsonArray = jsonObject.getJSONArray("maps");
            JSONObject jsonOneRecord = jsonArray.getJSONObject(curStage);
            if (!jsonOneRecord.isNull("bikeDensity"))
                answer = jsonOneRecord.getDouble("bikeDensity");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public double getBikeSpeed() {
        double answer = 0.03;
        try {
            if (!jsonObject.isNull("bikeSpeed"))
                answer = jsonObject.getDouble("bikeSpeed");
            JSONArray jsonArray = jsonObject.getJSONArray("maps");
            JSONObject jsonOneRecord = jsonArray.getJSONObject(curStage);
            if (!jsonOneRecord.isNull("bikeSpeed"))
                answer = jsonOneRecord.getDouble("bikeSpeed");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer * blockSize;
    }

    public double getBikeRadius() {
        double answer = 0.07;
        try {
            if (!jsonObject.isNull("bikeRadius"))
                answer = jsonObject.getDouble("bikeRadius");
            JSONArray jsonArray = jsonObject.getJSONArray("maps");
            JSONObject jsonOneRecord = jsonArray.getJSONObject(curStage);
            if (!jsonOneRecord.isNull("bikeRadius"))
                answer = jsonOneRecord.getDouble("bikeRadius");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer * blockSize;
    }

    public double getBikeMargin() {
        double answer = 0.2;
        try {
            if (!jsonObject.isNull("bikeMargin"))
                answer = jsonObject.getDouble("bikeMargin");
            JSONArray jsonArray = jsonObject.getJSONArray("maps");
            JSONObject jsonOneRecord = jsonArray.getJSONObject(curStage);
            if (!jsonOneRecord.isNull("bikeMargin"))
                answer = jsonOneRecord.getDouble("bikeMargin");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public double getReverseRunRatio() {
        double answer = 0.2;
        try {
            if (!jsonObject.isNull("reverseRunRatio"))
                answer = jsonObject.getDouble("reverseRunRatio");
            JSONArray jsonArray = jsonObject.getJSONArray("maps");
            JSONObject jsonOneRecord = jsonArray.getJSONObject(curStage);
            if (!jsonOneRecord.isNull("reverseRunRatio"))
                answer = jsonOneRecord.getDouble("reverseRunRatio");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public double getParallelRunRatio() {
        double answer = 0.0;
        try {
            if (!jsonObject.isNull("parallelRunRatio"))
                answer = jsonObject.getDouble("parallelRunRatio");
            JSONArray jsonArray = jsonObject.getJSONArray("maps");
            JSONObject jsonOneRecord = jsonArray.getJSONObject(curStage);
            if (!jsonOneRecord.isNull("parallelRunRatio"))
                answer = jsonOneRecord.getDouble("parallelRunRatio");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public double getSineCurveUnstabilityRatio() {
        double answer = 0.0;
        try {
            if (!jsonObject.isNull("sineCurveUnstabilityRatio"))
                answer = jsonObject.getDouble("sineCurveUnstabilityRatio");
            JSONArray jsonArray = jsonObject.getJSONArray("maps");
            JSONObject jsonOneRecord = jsonArray.getJSONObject(curStage);
            if (!jsonOneRecord.isNull("sineCurveUnstabilityRatio"))
                answer = jsonOneRecord.getDouble("sineCurveUnstabilityRatio");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public double getRandomWalkUnstabilityRatio() {
        double answer = 0.0;
        try {
            if (!jsonObject.isNull("randomWalkUnstabilityRatio"))
                answer = jsonObject.getDouble("randomWalkUnstabilityRatio");
            JSONArray jsonArray = jsonObject.getJSONArray("maps");
            JSONObject jsonOneRecord = jsonArray.getJSONObject(curStage);
            if (!jsonOneRecord.isNull("randomWalkUnstabilityRatio"))
                answer = jsonOneRecord.getDouble("randomWalkUnstabilityRatio");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public double getCarDensity() {
        double answer = 0;
        try {
            if (!jsonObject.isNull("carDensity"))
                answer = jsonObject.getDouble("carDensity");
            JSONArray jsonArray = jsonObject.getJSONArray("maps");
            JSONObject jsonOneRecord = jsonArray.getJSONObject(curStage);
            if (!jsonOneRecord.isNull("carDensity"))
                answer = jsonOneRecord.getDouble("carDensity");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public double getCarSpeed() {
        double answer = 0.03;
        try {
            if (!jsonObject.isNull("carSpeed"))
                answer = jsonObject.getDouble("carSpeed");
            JSONArray jsonArray = jsonObject.getJSONArray("maps");
            JSONObject jsonOneRecord = jsonArray.getJSONObject(curStage);
            if (!jsonOneRecord.isNull("carSpeed"))
                answer = jsonOneRecord.getDouble("carSpeed");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer * blockSize;
    }

    public double getCarRadius() {
        double answer = 0.07;
        try {
            if (!jsonObject.isNull("carRadius"))
                answer = jsonObject.getDouble("carRadius");
            JSONArray jsonArray = jsonObject.getJSONArray("maps");
            JSONObject jsonOneRecord = jsonArray.getJSONObject(curStage);
            if (!jsonOneRecord.isNull("carRadius"))
                answer = jsonOneRecord.getDouble("carRadius");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer * blockSize;
    }

    public double getCarMargin() {
        double answer = 0.2;
        try {
            if (!jsonObject.isNull("carMargin"))
                answer = jsonObject.getDouble("carMargin");
            JSONArray jsonArray = jsonObject.getJSONArray("maps");
            JSONObject jsonOneRecord = jsonArray.getJSONObject(curStage);
            if (!jsonOneRecord.isNull("carMargin"))
                answer = jsonOneRecord.getDouble("carMargin");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer;
    }

    private LabyrinthView.Callback callback;

    public Map(Context context, int width, int height, LabyrinthView.Callback cb, int stage, Resources res) {
        screenWidth = width;
        screenHeight = height;
        callback = cb;
        readJsonFile(context);
        stage = stage % stageNum;
        createMap(stage);
        createEdgeInfo();

        // セブンイレブンのBitmapをロード
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inScaled = false;
        sevenBitmap = BitmapFactory.decodeResource(res, R.drawable.seven, opt);
        sevenRect = new Rect(0, 0, sevenBitmap.getWidth(), sevenBitmap.getHeight());
    }

    int min(int x, int y) { return (x<=y) ? x : y; }

    // JSONファイルの読み込み
    void readJsonFile(Context context) {
        InputStream input;
        try {
            //input = new FileInputStream(Environment.getExternalStorageDirectory() + "/" +  "sample.json");
            input = context.getResources().openRawResource(R.raw.map);
            int size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();

            // Json読み込み
            String json = new String(buffer);
            json = json.replaceAll("┃","│");
            json = json.replaceAll("━","─");
            json = json.replaceAll("┫","┤");
            json = json.replaceAll("┣","├");
            json = json.replaceAll("┻","┴");
            json = json.replaceAll("┳","┬");
            json = json.replaceAll("╋","┼");
            jsonObject = new JSONObject(json);

            JSONArray jsonArray = jsonObject.getJSONArray("maps");
            stageNum = jsonArray.length();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // マップの作成
    void createMap(int stage) {
        stage = stage % stageNum;
        curStage = stage;
        try {
            // mapデータの設定
            JSONArray jsonArray = jsonObject.getJSONArray("maps");
            JSONObject jsonOneRecord = jsonArray.getJSONObject(stage);
            blockNumX = jsonOneRecord.getInt("blockNumX");
            blockNumY = jsonOneRecord.getInt("blockNumY");
            screenNumX = jsonOneRecord.getInt("screenNumX");
            screenNumY = jsonOneRecord.getInt("screenNumY");
            blockSize = min(screenWidth / blockNumX, screenHeight / blockNumY);
            String rawMap = jsonOneRecord.getString("map");

            //Log.v("SpeedFreak Log", jsonOneRecord.getString("blockNumX"));

            block = new Block[screenNumY][screenNumX][blockNumY][blockNumX];
            int counter = 0;
            for (int y = 0; y < screenNumY * blockNumY; y++) {
                int scY = y / blockNumY;
                int blY = y % blockNumY;
                for (int x = 0; x < screenNumX * blockNumX; x++) {
                    int scX = x / blockNumX;
                    int blX = x % blockNumX;
                    String ch;
                    do {
                        ch = rawMap.substring(counter, counter + 1);
                        counter++;
                    } while (ch.equals("\n") || ch.equals(" "));
                    int type = Block.TYPE_ROAD;
                    if ( ch.equals("＃")) { type = Block.TYPE_WALL; }
                    else if ( ch.equals("赤")) { type = Block.TYPE_RED_WALL; }
                    else if ( ch.equals("７")) { type = Block.TYPE_SEVEN; }
                    else if ( ch.equals("〇") || ch.equals("○")) { type = Block.TYPE_WARP; }
                    else if ( ch.equals("始")) { type = Block.TYPE_START; }
                    else if ( ch.equals("終")) { type = Block.TYPE_GOAL; }
                    int left = blX * blockSize + BLOCK_MARGIN_LEFT;
                    int top  = blY * blockSize + BLOCK_MARGIN_TOP;
                    int right  = left + blockSize - BLOCK_MARGIN_RIGHT;
                    int bottom = top  + blockSize - BLOCK_MARGIN_BOTTOM;
                    block[scY][scX][blY][blX] = new Block(type, left, top, right, bottom, ch);
                    if ( ch.equals("始")) {
                        startBlock = block[scY][scX][blY][blX];
                        curScreenX = scX;
                        curScreenY = scY;
                    }
                }
            }
            // スタート地点が存在しないときどうするか？


            //for (int i = 0; i < jsonArray.length(); i++) {
            //    JSONObject jsonOneRecord = jsonArray.getJSONObject(i);
            //    textView3.setText(jsonOneRecord.getString("Name"));
            //    Log.v("mytag", jsonOneRecord.getString("Name"));
            //    Log.v("mytag", String.valueOf(jsonOneRecord.getInt("Age")));
            //    Log.v("SpeedFreak Log", jsonOneRecord.getString("blockNumX"));
            //}
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // 自動車、自転車の移動経路に関する情報を生成
    void createEdgeInfo() {
        obstacleStart = new ArrayList[screenNumY][screenNumX];
        obstacleEnd   = new ArrayList[screenNumY][screenNumX];

        for (int scY = 0; scY < screenNumY; scY++) {
            for (int scX = 0; scX < screenNumX; scX++) {

                obstacleStart[scY][scX] = new ArrayList<Block>();
                obstacleEnd[scY][scX]   = new ArrayList<Block>();

                for (int blY = 0; blY < blockNumY; blY++) {
                    for (int blX = 0; blX < blockNumX; blX++) {
                        if (( block[scY][scX][blY][blX].hasEdgeInDir(Block.DIR_UP) && blY==0) ||
                            ( block[scY][scX][blY][blX].hasEdgeInDir(Block.DIR_UP) && blY>0 &&
                             !block[scY][scX][blY-1][blX].hasEdgeInDir(Block.DIR_DOWN)) ||
                            ( block[scY][scX][blY][blX].hasEdgeInDir(Block.DIR_DOWN) && blY==blockNumY-1) ||
                            ( block[scY][scX][blY][blX].hasEdgeInDir(Block.DIR_DOWN) && blY<blockNumY-1 &&
                             !block[scY][scX][blY+1][blX].hasEdgeInDir(Block.DIR_UP)) ||
                            ( block[scY][scX][blY][blX].hasEdgeInDir(Block.DIR_RIGHT) && blX==blockNumX-1) ||
                            ( block[scY][scX][blY][blX].hasEdgeInDir(Block.DIR_RIGHT) && blX<blockNumX-1 &&
                             !block[scY][scX][blY][blX+1].hasEdgeInDir(Block.DIR_LEFT)) ||
                            ( block[scY][scX][blY][blX].hasEdgeInDir(Block.DIR_LEFT) && blX==0 ) ||
                            ( block[scY][scX][blY][blX].hasEdgeInDir(Block.DIR_LEFT) && blX>0 &&
                             !block[scY][scX][blY][blX-1].hasEdgeInDir(Block.DIR_RIGHT))) {
                            if (block[scY][scX][blY][blX].getLetter().equals("←") ||
                                block[scY][scX][blY][blX].getLetter().equals("→") ||
                                block[scY][scX][blY][blX].getLetter().equals("↑") ||
                                block[scY][scX][blY][blX].getLetter().equals("↓")) {
                                obstacleEnd[scY][scX].add(block[scY][scX][blY][blX]);
                            } else {
                                obstacleStart[scY][scX].add(block[scY][scX][blY][blX]);
                            }
                        }
                    }
                }

                // 自動車・自転車の消失地点(obstacleEnd[][])から逆に辿る
                for (int i = 0; i < obstacleEnd[scY][scX].size(); i++) {
                    Block b = obstacleEnd[scY][scX].get(i);
                    int dir = Block.DIR_NONE;
                    int dx = 0;
                    int dy = 0;
                    if (b.getLetter().equals("←")) { dir = Block.DIR_RIGHT; dx=1; }
                    else if (b.getLetter().equals("→")) { dir = Block.DIR_LEFT; dx=-1; }
                    else if (b.getLetter().equals("↑")) { dir = Block.DIR_DOWN; dy=1; }
                    else if (b.getLetter().equals("↓")) { dir = Block.DIR_UP; dy=-1; }

                    while (true) {
                        int blY = getBlockY(b);
                        int blX = getBlockX(b);
                        int dirA = dir ^ 2;  // 上⇔左 もしくは 下⇔右
                        int dirB = dirA ^ 1; // その逆方向
                        if (b.hasEdgeInDir(dir)) { // 直進できるか？
                        } else if ((b.hasEdgeInDir(dirA) && !b.hasEdgeInDir(dirB)) ||
                                    (b.hasEdgeInDir(dirB) && !b.hasEdgeInDir(dirA))) { // 一本道かどうか？
                            int tmp;
                            tmp = dx; dx = dy; dy = tmp;
                            if (b.hasEdgeInDir(dirA)) {
                                dir = dirA;
                            } else {
                                dir = dirB;
                                dx = -dx;
                                dy = -dy;
                            }
                        }
                        else break;

                        if (blX+dx < 0 || blX+dx >= blockNumX || blY+dy < 0 || blY+dy >= blockNumY) break;
                        Block b2 = block[scY][scX][blY + dy][blX + dx];
                        int oppositeDir = dir ^ 1;
                        if (b2.hasEdgeInDir(oppositeDir)) {
                            b.setEdgeDir(dir, Block.INorOUT_IN);
                            b2.setEdgeDir(oppositeDir, Block.INorOUT_OUT);
                            b = b2;
                        }
                        else break;
                    }
                }

                // 自動車・自転車の出現地点(obstacleStart[][])から順番に辿る
                for (int i = 0; i < obstacleStart[scY][scX].size(); i++) {
                    Block b = obstacleStart[scY][scX].get(i);
                    int blY = getBlockY(b);
                    int blX = getBlockX(b);
                    int dir = Block.DIR_NONE;
                    int dx = 0;
                    int dy = 0;

                    if ( block[scY][scX][blY][blX].hasEdgeInDir(Block.DIR_UP) && blY>0 &&
                         block[scY][scX][blY-1][blX].hasEdgeInDir(Block.DIR_DOWN)) { dir = Block.DIR_UP; dy=-1; }
                    else if ( block[scY][scX][blY][blX].hasEdgeInDir(Block.DIR_DOWN) && blY<blockNumY-1 &&
                               block[scY][scX][blY+1][blX].hasEdgeInDir(Block.DIR_UP)) { dir = Block.DIR_DOWN; dy=1;}
                    else if ( block[scY][scX][blY][blX].hasEdgeInDir(Block.DIR_RIGHT) && blX<blockNumX-1 &&
                               block[scY][scX][blY][blX+1].hasEdgeInDir(Block.DIR_LEFT)) { dir = Block.DIR_RIGHT; dx=1; }
                    else if ( block[scY][scX][blY][blX].hasEdgeInDir(Block.DIR_LEFT) && blX>0 &&
                               block[scY][scX][blY][blX-1].hasEdgeInDir(Block.DIR_RIGHT)) { dir = Block.DIR_LEFT; dx=-1; }

                    while (true) {
                        blY = getBlockY(b);
                        blX = getBlockX(b);
                        int dirA = dir ^ 2;  // 上⇔左 もしくは 下⇔右
                        int dirB = dirA ^ 1; // その逆方向
                        if (b.hasEdgeInDir(dir)) { // 直進できるか？
                        } else if ((b.hasEdgeInDir(dirA) && !b.hasEdgeInDir(dirB)) ||
                                (b.hasEdgeInDir(dirB) && !b.hasEdgeInDir(dirA))) { // 一本道かどうか？
                            int tmp;
                            tmp = dx; dx = dy; dy = tmp;
                            if (b.hasEdgeInDir(dirA)) {
                                dir = dirA;
                            } else {
                                dir = dirB;
                                dx = -dx;
                                dy = -dy;
                            }
                        }
                        else break;

                        if ( b.getEdgeDir(dir) == Block.INorOUT_OUT) break; // 既に調査済み
                        if (blX+dx < 0 || blX+dx >= blockNumX || blY+dy < 0 || blY+dy >= blockNumY) break;
                        Block b2 = block[scY][scX][blY + dy][blX + dx];
                        int oppositeDir = dir ^ 1;
                        if (b2.hasEdgeInDir(oppositeDir)) {
                            b.setEdgeDir(dir, Block.INorOUT_OUT);
                            b2.setEdgeDir(oppositeDir, Block.INorOUT_IN);
                            b = b2;
                        }
                        else break;
                    }
                }


            }
        }
    }


    void drawMap(Canvas canvas) {
        for (int y = 0; y < blockNumY; y++) {
            for (int x = 0; x < blockNumX; x++) {
                block[curScreenY][curScreenX][y][x].draw(canvas);
            }
        }
    }

    // ゴール判定の反応が悪いのを改善
    @Override
    public void onSpecialBlock(int left, int top, int right, int bottom) {
        int verticalBlock = (top+bottom)/2 / blockSize;
        int horizontalBlock = (left+right)/2 / blockSize;

        // 検索対象のブロックを設定
        seTargetBlock(verticalBlock, horizontalBlock);

        int yLen = targetBlock.length;
        int xLen = targetBlock[0].length;

        for (int y = 1; y < 2; y++) {
            for (int x = 1; x < 2; x++) {
                if (targetBlock[y][x] == null) {
                    continue;
                }
                if (targetBlock[y][x].type == Block.TYPE_WARP
                        && targetBlock[y][x].rect.contains(left + 2, top + 2, right - 2, bottom - 2)) {
                    switchScreen(verticalBlock, horizontalBlock);
                } else if (targetBlock[y][x].type == Block.TYPE_GOAL
                        && targetBlock[y][x].rect.contains(left + 2, top + 2, right - 2, bottom - 2)) {
                    callback.onGoal();
                }
            }
        }
    }

    @Override
    public boolean canMove(int left, int top, int right, int bottom) {
        if (left < 0 || top < 0 || right >= blockNumX*blockSize || bottom >= blockNumY*blockSize ) return false;

        int verticalBlock = (top+bottom)/2 / blockSize;
        int horizontalBlock = (left+right)/2 / blockSize;

        // 検索対象のブロックを設定
        seTargetBlock(verticalBlock, horizontalBlock);

        int yLen = targetBlock.length;
        int xLen = targetBlock[0].length;

        for (int y = 0; y < yLen; y++) {
            for (int x = 0; x < xLen; x++) {
                if (targetBlock[y][x] == null) {
                    continue;
                }
                if (targetBlock[y][x].type >= Block.TYPE_WALL
                        && targetBlock[y][x].rect.intersects(left, top, right, bottom)) {
                    return false;
                }
                //else if (targetBlock[y][x].type == Block.TYPE_WARP
                //        && targetBlock[y][x].rect.contains(left, top, right, bottom)) {
                //    switchScreen(verticalBlock, horizontalBlock);
                //    return false; // 動けなかったとして扱う。さもないと、Ball.tryMoveHorizontal()でボールの位置を上書きしてしまう
                //} else if (targetBlock[y][x].type == Block.TYPE_GOAL
                //        && targetBlock[y][x].rect.contains(left, top, right, bottom)) {
                //    callback.onGoal();
                //    return true;
                //}
            }
        }
        return true;
    }

    private void seTargetBlock(int verticalBlock, int horizontalBlock) {
        targetBlock[1][1] = getBlock(verticalBlock, horizontalBlock);

        targetBlock[0][0] = getBlock(verticalBlock - 1, horizontalBlock - 1);
        targetBlock[0][1] = getBlock(verticalBlock - 1, horizontalBlock);
        targetBlock[0][2] = getBlock(verticalBlock - 1, horizontalBlock + 1);

        targetBlock[1][0] = getBlock(verticalBlock, horizontalBlock - 1);
        targetBlock[1][2] = getBlock(verticalBlock, horizontalBlock + 1);

        targetBlock[2][0] = getBlock(verticalBlock + 1, horizontalBlock - 1);
        targetBlock[2][1] = getBlock(verticalBlock + 1, horizontalBlock);
        targetBlock[2][2] = getBlock(verticalBlock + 1, horizontalBlock + 1);
    }

    public Block getBlock(int y, int x) {
        if (y < 0 || x < 0 || y >= blockNumY || x >= blockNumX) {
            return null;
        }
        return block[curScreenY][curScreenX][y][x];
    }

    // 上下左右のスクリーンへ表示を切り替え
    private void switchScreen(int verticalBlock, int horizontalBlock) {
        if (horizontalBlock == 0) { // 左のスクリーンへ移動
            curScreenX = (curScreenX + (screenNumX-1)) % screenNumX;
            if (ball != null) ball.forceMoveRightMost(blockNumX*blockSize-1);
        }
        else if (horizontalBlock == blockNumX-1) { // 右のスクリーンへ移動
            curScreenX = (curScreenX + 1) % screenNumX;
            if (ball != null) ball.forceMoveLeftMost(0);
        }
        else if (verticalBlock == 0) { // 上のスクリーンへ移動
            curScreenY = (curScreenY + (screenNumY-1)) % screenNumY;
            if (ball != null) ball.forceMoveBottomMost(blockNumY*blockSize-1);
        }
        else if (verticalBlock == blockNumY-1) { // 下のスクリーンへ移動
            curScreenY = (curScreenY + 1) % screenNumY;
            if (ball != null) ball.forceMoveTopMost(0);
        }
    }


    static class Block {

        private static final int TYPE_ROAD = 0;
        private static final int TYPE_WARP = 1;
        private static final int TYPE_START = 2;
        private static final int TYPE_GOAL = 3;
        private static final int TYPE_WALL = 4;
        private static final int TYPE_RED_WALL = 5;
        private static final int TYPE_SEVEN = 6;

        private static final Paint PAINT_ROAD = new Paint();
        private static final Paint PAINT_WARP = new Paint();
        private static final Paint PAINT_WALL = new Paint();
        private static final Paint PAINT_START = new Paint();
        private static final Paint PAINT_GOAL = new Paint();
        private static final Paint PAINT_RED_WALL = new Paint();

        static {
            PAINT_ROAD.setColor(Color.GRAY);
            PAINT_WARP.setColor(Color.WHITE);
            PAINT_WALL.setColor(Color.rgb(0,0,64));
            PAINT_START.setColor(Color.DKGRAY);
            PAINT_GOAL.setColor(Color.YELLOW);
            PAINT_RED_WALL.setColor(Color.RED);
        }

        public static final int DIR_UP = 0;
        public static final int DIR_DOWN = 1;
        public static final int DIR_LEFT = 2;
        public static final int DIR_RIGHT = 3;
        public static final int DIR_NONE = 4;

        public static final int INorOUT_IN = 0;
        public static final int INorOUT_OUT = 1;
        public static final int INorOUT_NONE = 2;

        private final int type;
        final Rect rect;
        final String letter;
        public boolean ifTypeRoad() { return (type == TYPE_ROAD); }
        public boolean ifNormalRoad() {
            return (ifTypeRoad() && !letter.equals("始") && !letter.equals("終") && !letter.equals("○") && !letter.equals("〇") &&
                    !letter.equals("↑") && !letter.equals("↓") && !letter.equals("←") && !letter.equals("→"));
        }
        public String getLetter() { return letter; }

        private boolean[] edge = {false, false, false, false};
        private int[] edgeDir = {INorOUT_NONE, INorOUT_NONE, INorOUT_NONE, INorOUT_NONE};

        public boolean hasEdgeUP(String letter) {
            return (letter.equals("│") || letter.equals("└") || letter.equals("┘") ||
                    letter.equals("┤")|| letter.equals("├")|| letter.equals("┴")|| letter.equals("┼") ||
                    letter.equals("↑") || letter.equals("↓"));
        }
        public boolean hasEdgeDOWN(String letter) {
            return (letter.equals("│") || letter.equals("┌") || letter.equals("┐") ||
                    letter.equals("┤")|| letter.equals("├")|| letter.equals("┬")|| letter.equals("┼") ||
                    letter.equals("↑") || letter.equals("↓"));
        }
        public boolean hasEdgeRIGHT(String letter) {
            return (letter.equals("─") || letter.equals("└") || letter.equals("┌") ||
                    letter.equals("┬")|| letter.equals("├")|| letter.equals("┴")|| letter.equals("┼") ||
                    letter.equals("→") || letter.equals("←"));
        }
        public boolean hasEdgeLEFT(String letter) {
            return (letter.equals("─") || letter.equals("┐") || letter.equals("┘") ||
                    letter.equals("┬")|| letter.equals("┤")|| letter.equals("┴")|| letter.equals("┼") ||
                    letter.equals("→") || letter.equals("←"));
        }


        private Block(int type, int left, int top, int right, int bottom, String letter) {
            this.type = type;
            rect = new Rect(left, top, right, bottom);
            this.letter = letter;
            edge[DIR_UP]    = hasEdgeUP(letter);
            edge[DIR_DOWN]  = hasEdgeDOWN(letter);
            edge[DIR_RIGHT] = hasEdgeRIGHT(letter);
            edge[DIR_LEFT]  = hasEdgeLEFT(letter);
        }

        public boolean hasEdgeInDir(int dir) { return edge[dir]; }
        private void    setEdgeDir(int dir, int io) { edgeDir[dir] = io; }
        public  int     getEdgeDir(int dir)         { return edgeDir[dir]; }

        public int selectOutDirection() {
            ArrayList<Integer> candidates = new ArrayList<Integer>();
            for (int dir = 0; dir < 4; dir++) { // forall Map.Block.DIR_UP, DOWN, LEFT, RIGHT
                if (getEdgeDir(dir) == Map.Block.INorOUT_OUT) candidates.add(dir);
            }
            if (candidates.size() == 0)
                return DIR_NONE;
            if (candidates.size() == 1)
                return candidates.get(0);
            return candidates.get( random.nextInt(candidates.size()));
        }
        public int selectInDirection() {
            ArrayList<Integer> candidates = new ArrayList<Integer>();
            for (int dir = 0; dir < 4; dir++) { // forall Map.Block.DIR_UP, DOWN, LEFT, RIGHT
                if (getEdgeDir(dir) == Map.Block.INorOUT_IN) candidates.add(dir);
            }
            if (candidates.size() == 0)
                return DIR_NONE;
            if (candidates.size() == 1)
                return candidates.get(0);
            return candidates.get( random.nextInt(candidates.size()));
        }

        private Paint getPaint() {
            switch (type) {
                case TYPE_ROAD:
                    return PAINT_ROAD;
                case TYPE_WARP:
                    return PAINT_WARP;
                case TYPE_START:
                    return PAINT_START;
                case TYPE_GOAL:
                    return PAINT_GOAL;
                case TYPE_WALL:
                    return PAINT_WALL;
                case TYPE_RED_WALL:
                    return PAINT_RED_WALL;
            }
            return null;
        }

        private void draw(Canvas canvas) {
            if (type == TYPE_SEVEN)
                canvas.drawBitmap(sevenBitmap, sevenRect, rect, getPaint());
            else
                canvas.drawRect(rect, getPaint());

/*
            float cx = (rect.left + rect.right) / 2;
            float cy = (rect.top + rect.bottom) / 2;
            int size = rect.height();
            // 外に出るedgeを赤で着色
            Paint outEdgeColor = new Paint(); outEdgeColor.setColor(Color.RED);
            if (getEdgeDir(DIR_UP) == INorOUT_OUT)
                canvas.drawCircle(cx, cy - size*0.3f, 7, outEdgeColor);
            if (getEdgeDir(DIR_DOWN) == INorOUT_OUT)
                canvas.drawCircle(cx, cy + size*0.3f, 7, outEdgeColor);
            if (getEdgeDir(DIR_LEFT) == INorOUT_OUT)
                canvas.drawCircle(cx - size*0.3f, cy, 7, outEdgeColor);
            if (getEdgeDir(DIR_RIGHT) == INorOUT_OUT)
                canvas.drawCircle(cx + size*0.3f, cy, 7, outEdgeColor);
            // 内に入ってくるedgeを青で着色
            Paint inEdgeColor = new Paint(); inEdgeColor.setColor(Color.BLUE);
            if (getEdgeDir(DIR_UP) == INorOUT_IN)
                canvas.drawCircle(cx, cy - size*0.4f, 7, inEdgeColor);
            if (getEdgeDir(DIR_DOWN) == INorOUT_IN)
                canvas.drawCircle(cx, cy + size*0.4f, 7, inEdgeColor);
            if (getEdgeDir(DIR_LEFT) == INorOUT_IN)
                canvas.drawCircle(cx - size*0.4f, cy, 7, inEdgeColor);
            if (getEdgeDir(DIR_RIGHT) == INorOUT_IN)
                canvas.drawCircle(cx + size*0.4f, cy, 7, inEdgeColor);
*/
        }

    }
}
